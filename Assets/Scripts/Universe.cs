﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Universe : MonoBehaviour
{

    public float recycledResources;
    public int startingResources;

    [System.Serializable]
    public class ScatteredResources
    {
        public float amount;
        public int maxAmount;
        public int increment;
    };

    public List<ScatteredResources> resourcesList = new List<ScatteredResources>();

    // Use this for initialization
    void Start()
    {
        while (startingResources > 0)
        {
            GameObject I = Instantiate(Resources.Load("resources/resources")) as GameObject;

            I.transform.parent = transform;
            I.transform.position = new Vector3(Random.Range(-100, 101), Random.Range(-100, 101), Random.Range(-100, 101));

            I.GetComponent<ResourcePile>().amount += 20;
            //gonna create different models or graphics for different scales & shapes
            //I.transform.localScale = new Vector3(Random.Range(0.2f, 0.6f), Random.Range(0.2f, 0.6f), Random.Range(0.2f, 0.6f)); //randomizing scale and shape

            startingResources--;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //recycled resources gathers to form a pile of resources
        if (recycledResources >= 0)
        {
            recycledResources -= Time.deltaTime * 6;

            foreach (ScatteredResources resource in resourcesList)
            {
                resource.amount += Time.deltaTime * resource.increment;
            }
        }

        foreach (ScatteredResources resource in resourcesList)
        {
            if (resource.amount >= resource.maxAmount)
            {
                GatheredResourceSpawn(resource.maxAmount);
                resource.amount = 0;
            }
        }

        if (transform.GetChild(0).transform.childCount == 0) transform.GetComponentInChildren<ResourcePile>().formLife = true;
    }

    void GatheredResourceSpawn(int amount)
    {
        
        GameObject I = Instantiate(Resources.Load("resources/resources")) as GameObject;

        I.transform.parent = transform;
        I.transform.position = new Vector3(Random.Range(-100, 101), Random.Range(-100, 101), Random.Range(-100, 101));
        //to-do: different models / graphics (2D?) according to amount

        I.GetComponent<ResourcePile>().amount += amount;
    }
}
