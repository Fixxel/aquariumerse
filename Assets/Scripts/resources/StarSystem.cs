﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystem : MonoBehaviour {

    //
    //might just stick to resourcepile system and scrap this
    float sysRotation;

	// Use this for initialization
	void Start () {
        sysRotation = Random.Range(40f, 80f);
        transform.eulerAngles = new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180));
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.left * sysRotation * Time.deltaTime);
        transform.Rotate(Vector3.up * sysRotation * Time.deltaTime);
    }
}
