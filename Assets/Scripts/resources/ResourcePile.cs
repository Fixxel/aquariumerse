﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePile : MonoBehaviour {

    public bool formLife;

    public float amount;
    
    float timer;

    // Use this for initialization
    void Start()
    {
        transform.parent = GameObject.Find("ResourceContainer").transform;

        name = "resources";
        timer = Random.Range(30, 10000);
    }

    // Update is called once per frame
    void Update()
    {
        if (formLife)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            GameObject load = Resources.Load("lifeforms/Wanderer") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = transform.position;
        }

        if (amount <= 0) Destroy(gameObject);
    }
    
}
