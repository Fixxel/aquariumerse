﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour {

    //
    //might just stick to resourcepile system and scrap this
    // Use this for initialization
    void Start () {
        transform.eulerAngles = new Vector3(Random.Range(0,180), Random.Range(0, 180), Random.Range(0, 180));
        
        int planets = Random.Range(1, 3);
        float minPDist = 1.5f;
        float maxPDist = 1f;

        while (planets > 0)
        {
            GameObject I = Instantiate(Resources.Load("resources/Planet")) as GameObject;
            I.transform.parent = transform;
            I.transform.position = transform.position;
            I.transform.position += new Vector3(0, Random.Range(minPDist, maxPDist), 0);

            maxPDist += 0.5f;
            minPDist += 0.5f;
            planets--;
        }
	}
	
	// Update is called once per frame
	void Update () {
    }
}
