﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {

    float orbitPeriod;
    //
    //might just stick to resourcepile system and scrap this
    // Use this for initialization
    void Start () {
        orbitPeriod = Random.Range(25f, 30f);
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(transform.parent.position, Vector3.up, orbitPeriod * Time.deltaTime);
        transform.RotateAround(transform.parent.position, Vector3.left, orbitPeriod * Time.deltaTime);
        transform.RotateAround(transform.parent.position, Vector3.forward, orbitPeriod * Time.deltaTime);
    }
}
