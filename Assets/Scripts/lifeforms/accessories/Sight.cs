﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour {

    public bool lockedOnResources;

    //public bool lockedOnLifeform;

    //public float sightedResourceAmount;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "resources" && lockedOnResources == false)
        {
            lockedOnResources = true;
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.transform.tag == "resources" && lockedOnResources)
        {
            GetComponentInParent<Wanderer>().SightOnResources(col.transform.position);
        }
    }

    private void OnTriggerExit(Collider col)
    {

    }
}
