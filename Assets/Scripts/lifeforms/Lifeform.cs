﻿using UnityEngine;
using System.Collections.Generic;

public class Lifeform : MonoBehaviour
{
    public bool First;

    public float minAggression;     //set in inspector
    public float maxAggression;     //set in inspector

    public float Aggression;

    public float minGathering;      //set in inspector
    public float maxGathering;      //set in inspector
    public float Gathering;

    public float minHealthCap;      //set in inspector
    public float maxHealthCap;      //set in inspector
    public float HealthCap;
    public float Health;            //comes from healthcap on start

    public float minMovement;       //set in inspector
    public float maxMovement;       //set in inspector
    public float Movement;

    public float Reproduction;      //always starts at 0, builds up from storedresources

    public float minStrength;       //set in inspector
    public float maxStrength;       //set in inspector
    public float Strength;

    public float StoredResources;   //comes from place of origin or inherited

    public float minSight;          //set in inspector
    public float maxSight;          //set in inspector
    public float Sight;

    public float Form;              //inherited

    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }
}
