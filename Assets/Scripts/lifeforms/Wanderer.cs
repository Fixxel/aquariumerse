﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wanderer : Lifeform
{

    bool targetGot;
    Vector3 target;

    // Use this for initialization
    private void Awake()
    {
        if (First)  //if first of its kind (not inheriting), random range these
        {
            Aggression = Random.Range(minAggression, maxAggression);
            Strength = Random.Range(minStrength, maxStrength);
            Sight = Random.Range(minSight, maxSight);
            Form = Random.Range(1, 1001);
        }

        Gathering = Random.Range(minGathering, maxGathering);
        HealthCap = Random.Range(minHealthCap, maxHealthCap);
        Health = HealthCap;
        Movement = Random.Range(minMovement, maxMovement);
    }

    void Start() {
        name = "wanderer";
        transform.parent = GameObject.Find("LifeformContainer").transform;
        transform.eulerAngles = new Vector3(Random.Range(0, 181), Random.Range(0, 181), Random.Range(0, 181));

        transform.GetChild(0).GetComponent<SphereCollider>().radius = Sight;
    }

    // Update is called once per frame
    void Update() {

        //lifeform movement
        transform.Translate(Vector3.forward * Movement * Time.deltaTime);

        //lifeform rotation
        int LeftOrRight = Random.Range(0, 2);
        transform.Rotate(LeftOrRight > 0 ? Vector3.left * Random.Range(0, 180) * Time.deltaTime : Vector3.right * Random.Range(0, 180) * Time.deltaTime);

        int UpOrDown = Random.Range(0, 2);
        transform.Rotate(UpOrDown > 0 ? Vector3.up * Random.Range(0, 180) * Time.deltaTime : Vector3.down * Random.Range(0, 180) * Time.deltaTime);

        //preventing exit from universe bounds
        if (transform.position.x > 100) { transform.position = new Vector3(-100, transform.position.y, transform.position.z); }
        if (transform.position.x < -100) { transform.position = new Vector3(100, transform.position.y, transform.position.z); }
        if (transform.position.y > 100) { transform.position = new Vector3(transform.position.x, -100, transform.position.z); }
        if (transform.position.y < -100) { transform.position = new Vector3(transform.position.x, 100, transform.position.z); }
        if (transform.position.z > 100) { transform.position = new Vector3(transform.position.x, transform.position.y, -100); }
        if (transform.position.z < -100) { transform.position = new Vector3(transform.position.x, transform.position.y, 100); }

        //for clearing railrenderer when closing edge/loop of the universe
        if ((transform.position.x > 99.9f || transform.position.x < -99.9f) || (transform.position.y > 99.9f || transform.position.y < -99.9f) || (transform.position.z > 99.9f || transform.position.z < -99.9f))
        {
            GetComponent<TrailRenderer>().Clear();
        }

        //lifeform resource consumption
        if (StoredResources > 0)      //using resouces to reproduction
        {
            StoredResources -= Time.deltaTime;

            transform.parent.GetComponentInParent<Universe>().recycledResources += Time.deltaTime;

            Reproduction += Time.deltaTime;
        }
        if (Reproduction >= Health)     //health used to measure how much needed to reproduce another lifeform
        {
            Reproduce(Form, Aggression, StoredResources / 2, Strength, Sight);
            StoredResources = StoredResources / 2;

            Reproduction = 0;
        }
        if (StoredResources > 0 && Health < HealthCap)      //recovering from hunger when there are resources (and if damage was taken from hunger ... or any other source of damage)
        {
            StoredResources -= Time.deltaTime;
            Health += Time.deltaTime;
        }


        //lifeform hunger
        if (StoredResources <= 0) Health -= Time.deltaTime;                       //hunger when resources run out
        if (Health <= 0) Destroy(gameObject);
    }
    

    private void OnCollisionStay(Collision col)   //collects resources on contact, might change it not to be instantaneous
    {
        if (col.transform.tag == "resources")
        {
            StoredResources += col.transform.GetComponent<ResourcePile>().amount;
            transform.GetChild(0).GetComponent<Sight>().lockedOnResources = false;
            Destroy(col.gameObject);
        }
    }

    public void SightOnResources(Vector3 resLoc)    //for approaching locked resource, and removal of the lock once target position is reached
    {
        transform.LookAt(resLoc);
    }

    private void SightOnLifeform()
    {

    }

    private void Reproduce(float inheritType, float inheritAggression, float inheritResources, float inheritStrength, float inheritSight)
    {
        GameObject L = Resources.Load("lifeforms/wandererOffspring") as GameObject;
        GameObject I = Instantiate(L) as GameObject;

        I.transform.position = transform.position;
        
        I.transform.GetComponent<Lifeform>().Form = inheritType;
        I.transform.GetComponent<Lifeform>().Aggression = inheritAggression;
        I.transform.GetComponent<Lifeform>().StoredResources = inheritResources;
        I.transform.GetComponent<Lifeform>().Strength = inheritStrength;
        I.transform.GetComponent<Lifeform>().Sight = inheritSight;
    }

}
