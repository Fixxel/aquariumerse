﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{

    public Vector3 universePos = new Vector3(0, 0, 0);

    // Use this for initialization
    void Start()
    {
        transform.LookAt(universePos);
    }

    // Update is called once per frame
    void Update()
    {


        //camera rotation for Mobile
        //if (Input.touchCount <= 1 && Input.GetTouch(0).position.y > Screen.height / 2 + 200)
        //{
        //    transform.Translate(Vector3.up * 100 * Time.deltaTime);
        //    transform.LookAt(universePos);
        //}
        //else if (Input.touchCount <= 1 && Input.GetTouch(0).position.y < Screen.height / 2 - 200)
        //{
        //    transform.Translate(Vector3.down * 100 * Time.deltaTime);
        //    transform.LookAt(universePos);
        //}

        //if (Input.touchCount <= 1 && Input.GetTouch(0).position.x > Screen.width / 2 + 200)
        //{
        //    transform.Translate(Vector3.right * 100 * Time.deltaTime);
        //    transform.LookAt(universePos);
        //}
        //else if (Input.touchCount <= 1 && Input.GetTouch(0).position.x < Screen.width / 2 - 200)
        //{
        //    transform.Translate(Vector3.left * 100 * Time.deltaTime);
        //    transform.LookAt(universePos);
        //}

        //camera rotation for PC
        if (Input.GetKey(KeyCode.UpArrow) && transform.position.y <= 180)
        {
            transform.Translate(Vector3.up * 100 * Time.deltaTime);
            transform.LookAt(universePos);
        }
        else if (Input.GetKey(KeyCode.DownArrow) && transform.position.y >= -180)
        {
            transform.Translate(Vector3.down * 100 * Time.deltaTime);
            transform.LookAt(universePos);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left * 100 * Time.deltaTime);
            transform.LookAt(universePos);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * 100 * Time.deltaTime);
            transform.LookAt(universePos);
        }
    }
}
